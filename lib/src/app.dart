import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';

import 'bl/blocs/movie_detail_provider.dart';
import 'bl/blocs/movie_search_provider.dart';
import 'bl/blocs/splash_provider.dart';
import 'bl/blocs/theme_provider.dart';
import 'ui/pages/movie_detail_page.dart';
import 'ui/pages/movie_search_page.dart';
import 'ui/pages/splash_page.dart';
import 'utils/colors.dart';
import 'utils/strings.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => MovieDetailProvider()),
        ChangeNotifierProvider(create: (context) => MovieSearchProvider()),
        ChangeNotifierProvider(create: (context) => SplashProvider()),
        ChangeNotifierProvider(create: (context) => ThemeProvider()),
      ],
      child: _MyMaterialApp(),
    );
  }
}

class _MyMaterialApp extends StatefulWidget {
  @override
  State<_MyMaterialApp> createState() => _MyMaterialAppState();
}

class _MyMaterialAppState extends State<_MyMaterialApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: sAppName,
      debugShowCheckedModeBanner: false,
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: const [Locale('es')],
      initialRoute: SplashPage.route,
      routes: {
        SplashPage.route: (_) => const SplashPage(),
        MovieSearchPage.route: (_) => const MovieSearchPage(),
        MovieDetailPage.route: (_) => const MovieDetailPage(),
      },
      themeMode: ThemeMode.system,
      theme: ThemeData(
        //accentColor: secondaryColor, // Deprecated.
        appBarTheme: const AppBarTheme(
          actionsIconTheme: IconThemeData(color: cWhiteColor),
          backgroundColor: cPrimaryColor,
          centerTitle: true,
          elevation: 2,
          iconTheme: IconThemeData(color: cWhiteColor),
          foregroundColor: cWhiteColor,
          systemOverlayStyle: SystemUiOverlayStyle(
            statusBarColor: cTransparentColor,
            statusBarBrightness: Brightness.dark, // iOS (dark = letras e íconos en blanco).
            statusBarIconBrightness: Brightness.light, // Android (light = letras e íconos en blanco).
          ),
          titleTextStyle: TextStyle(color: cWhiteColor, fontFamily: 'OpenSans', fontSize: 20, fontWeight: FontWeight.w700),
          toolbarHeight: 60,
        ),
        cardTheme: const CardTheme(
          color: cWhiteColor,
          elevation: 3,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(8)), side: BorderSide(color: cTransparentColor)),
          surfaceTintColor: cWhiteColor,
        ),
        colorScheme: const ColorScheme.light().copyWith(brightness: Brightness.light, primary: cPrimaryColor, secondary: cSecondaryColor),
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(cPrimaryColor),
            elevation: MaterialStateProperty.all(2),
            foregroundColor: MaterialStateProperty.all(cPrimaryColor),
            padding: MaterialStateProperty.all(EdgeInsets.zero),
            shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(8))),
            side: MaterialStateProperty.all(BorderSide.none),
            textStyle: MaterialStateProperty.all(const TextStyle(fontFamily: 'OpenSans')),
          ),
        ),
        floatingActionButtonTheme: const FloatingActionButtonThemeData(
          backgroundColor: cPrimaryColor,
          foregroundColor: cSecondaryColor,
          highlightElevation: 0,
        ),
        fontFamily: 'OpenSans',
        inputDecorationTheme: InputDecorationTheme(
          contentPadding: const EdgeInsets.all(8),
          disabledBorder: const OutlineInputBorder(borderRadius: BorderRadius.only(topLeft: Radius.circular(8), bottomLeft: Radius.circular(8)), borderSide: BorderSide(style: BorderStyle.none)),
          enabledBorder: const OutlineInputBorder(borderRadius: BorderRadius.only(topLeft: Radius.circular(8), bottomLeft: Radius.circular(8)), borderSide: BorderSide(style: BorderStyle.none)),
          errorStyle: const TextStyle(color: cErrorColor),
          errorBorder: const OutlineInputBorder(borderRadius: BorderRadius.only(topLeft: Radius.circular(8), bottomLeft: Radius.circular(8)), borderSide: BorderSide(style: BorderStyle.none)),
          fillColor: cWhiteColor,
          filled: true,
          focusedBorder: const OutlineInputBorder(borderRadius: BorderRadius.only(topLeft: Radius.circular(8), bottomLeft: Radius.circular(8)), borderSide: BorderSide(style: BorderStyle.none)),
          focusedErrorBorder: const OutlineInputBorder(borderRadius: BorderRadius.only(topLeft: Radius.circular(8), bottomLeft: Radius.circular(8)), borderSide: BorderSide(style: BorderStyle.none)),
          hintStyle: const TextStyle(color: cGreyColor, fontWeight: FontWeight.w300),
          labelStyle: TextStyle(color: cTextLightColor, fontWeight: FontWeight.normal),
        ),
        outlinedButtonTheme: OutlinedButtonThemeData(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(cTransparentColor),
            elevation: MaterialStateProperty.all(0),
            foregroundColor: MaterialStateProperty.all(cPrimaryColor),
            shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(8))),
            side: MaterialStateProperty.all(const BorderSide(color: cPrimaryColor)),
            textStyle: MaterialStateProperty.all(const TextStyle(color: cPrimaryColor, fontFamily: 'OpenSans')),
          ),
        ),
        primaryColor: cPrimaryColor,
        scaffoldBackgroundColor: cScaffoldColor,
        snackBarTheme: const SnackBarThemeData(contentTextStyle: TextStyle(fontFamily: 'OpenSans')),
        textButtonTheme: TextButtonThemeData(style: ButtonStyle(foregroundColor: MaterialStateProperty.all(cPrimaryColor))),
        textSelectionTheme: const TextSelectionThemeData(cursorColor: cPrimaryColor, selectionColor: cSelectionLightColor),
        textTheme: TextTheme(
          bodyText1: TextStyle(color: cTextLightColor),
          bodyText2: TextStyle(color: cTextLightColor),
          button: TextStyle(color: cTextLightColor),
          caption: TextStyle(color: cTextLightColor),
          headline1: TextStyle(color: cTextLightColor),
          headline2: TextStyle(color: cTextLightColor),
          headline3: TextStyle(color: cTextLightColor),
          headline4: TextStyle(color: cTextLightColor),
          headline5: TextStyle(color: cTextLightColor),
          headline6: TextStyle(color: cTextLightColor),
          overline: TextStyle(color: cTextLightColor),
          subtitle1: TextStyle(color: cTextLightColor),
          subtitle2: TextStyle(color: cTextLightColor),
        ),
        useMaterial3: true,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      darkTheme: ThemeData(
        //accentColor: secondaryColor, // Deprecated.
        appBarTheme: const AppBarTheme(
          actionsIconTheme: IconThemeData(color: cWhiteColor),
          backgroundColor: cPrimaryColor,
          centerTitle: true,
          elevation: 2,
          iconTheme: IconThemeData(color: cWhiteColor),
          foregroundColor: cWhiteColor,
          systemOverlayStyle: SystemUiOverlayStyle(
            statusBarColor: cTransparentColor,
            statusBarBrightness: Brightness.dark, // iOS (dark = letras e íconos en blanco).
            statusBarIconBrightness: Brightness.light, // Android (light = letras e íconos en blanco).
          ),
          titleTextStyle: TextStyle(color: cWhiteColor, fontFamily: 'OpenSans', fontSize: 20, fontWeight: FontWeight.w700),
          toolbarHeight: 60,
        ),
        cardTheme: const CardTheme(
          color: cWhiteColor,
          elevation: 3,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(8)), side: BorderSide(color: cTransparentColor)),
          surfaceTintColor: cWhiteColor,
        ),
        colorScheme: const ColorScheme.light().copyWith(brightness: Brightness.light, primary: cPrimaryColor, secondary: cSecondaryColor),
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(cPrimaryColor),
            elevation: MaterialStateProperty.all(2),
            foregroundColor: MaterialStateProperty.all(cPrimaryColor),
            padding: MaterialStateProperty.all(EdgeInsets.zero),
            shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(8))),
            side: MaterialStateProperty.all(BorderSide.none),
            textStyle: MaterialStateProperty.all(const TextStyle(fontFamily: 'OpenSans')),
          ),
        ),
        floatingActionButtonTheme: const FloatingActionButtonThemeData(
          backgroundColor: cPrimaryColor,
          foregroundColor: cSecondaryColor,
          highlightElevation: 0,
        ),
        fontFamily: 'OpenSans',
        inputDecorationTheme: InputDecorationTheme(
          contentPadding: const EdgeInsets.all(8),
          disabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(8), borderSide: const BorderSide(style: BorderStyle.none)),
          enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(8), borderSide: const BorderSide(style: BorderStyle.none)),
          errorStyle: const TextStyle(color: cErrorColor),
          errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(8), borderSide: const BorderSide(style: BorderStyle.none)),
          fillColor: cWhiteColor,
          filled: true,
          focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(8), borderSide: const BorderSide(style: BorderStyle.none)),
          focusedErrorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(8), borderSide: const BorderSide(style: BorderStyle.none)),
          hintStyle: const TextStyle(color: cGreyColor, fontWeight: FontWeight.w300),
          labelStyle: TextStyle(color: cTextLightColor, fontWeight: FontWeight.normal),
        ),
        outlinedButtonTheme: OutlinedButtonThemeData(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(cTransparentColor),
            elevation: MaterialStateProperty.all(0),
            foregroundColor: MaterialStateProperty.all(cPrimaryColor),
            shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(8))),
            side: MaterialStateProperty.all(const BorderSide(color: cPrimaryColor)),
            textStyle: MaterialStateProperty.all(const TextStyle(color: cPrimaryColor, fontFamily: 'OpenSans')),
          ),
        ),
        primaryColor: cPrimaryColor,
        scaffoldBackgroundColor: cScaffoldColor,
        snackBarTheme: const SnackBarThemeData(contentTextStyle: TextStyle(fontFamily: 'OpenSans')),
        textButtonTheme: TextButtonThemeData(style: ButtonStyle(foregroundColor: MaterialStateProperty.all(cPrimaryColor))),
        textSelectionTheme: const TextSelectionThemeData(cursorColor: cPrimaryColor, selectionColor: cSelectionLightColor),
        textTheme: TextTheme(
          bodyText1: TextStyle(color: cTextLightColor),
          bodyText2: TextStyle(color: cTextLightColor),
          button: TextStyle(color: cTextLightColor),
          caption: TextStyle(color: cTextLightColor),
          headline1: TextStyle(color: cTextLightColor),
          headline2: TextStyle(color: cTextLightColor),
          headline3: TextStyle(color: cTextLightColor),
          headline4: TextStyle(color: cTextLightColor),
          headline5: TextStyle(color: cTextLightColor),
          headline6: TextStyle(color: cTextLightColor),
          overline: TextStyle(color: cTextLightColor),
          subtitle1: TextStyle(color: cTextLightColor),
          subtitle2: TextStyle(color: cTextLightColor),
        ),
        useMaterial3: true,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
    );
  }
}

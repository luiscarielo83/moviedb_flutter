class MovieModel {
  MovieModel({this.title, this.year, this.released, this.type, this.director, this.seasons, this.poster});

  String? title = '';
  String? year = '';
  String? released = '';
  String? type = '';
  String? director = '';
  String? seasons = '';
  String? poster = '';

  factory MovieModel.fromJson(Map<String, dynamic> json) => MovieModel(
        title: json["Title"],
        year: json["Year"],
        released: json["Released"],
        type: json["Type"],
        director: json["Director"],
        seasons: json["totalSeasons"],
        poster: json["Poster"],
      );

  Map<String, dynamic> toJson() => {
        "Title": title,
        "Year": year,
        "Released": released,
        "Type": type,
        "Director": director,
        "totalSeasons": seasons,
        "Poster": poster,
      };
}

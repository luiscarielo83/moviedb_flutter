import 'package:flutter/material.dart';

import '../../ui/pages/movie_search_page.dart';
import '../../utils/strings.dart';

class SplashProvider extends ChangeNotifier {
  final _className = 'SplashProvider';

  void moveToMainPage(BuildContext context) async {
    myDebugPrint(_className, 'MOVING TO MOVIE SEARCH PAGE...');
    Future.delayed(const Duration(milliseconds: 3000), () => Navigator.pushReplacementNamed(context, MovieSearchPage.route));
  }
}
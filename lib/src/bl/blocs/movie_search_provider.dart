import 'package:flutter/material.dart';

import '../../ui/pages/movie_detail_page.dart';
import '../../ui/widgets/my_alerts.dart';
import '../../utils/colors.dart';
import '../../utils/strings.dart';
import '../models/movie_model.dart';
import '../services/api_services/omdb_service.dart';

class MovieSearchProvider extends ChangeNotifier {
  final _className = 'MovieSearchProvider';
  final _alert = MyAlerts();
  final _api = OmdbService();
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  FocusNode _movieNode = FocusNode();
  TextEditingController _movieController = TextEditingController();
  MovieModel _movie = MovieModel();

  GlobalKey<FormState> get formKey => _formKey;

  set formKey(GlobalKey<FormState> value) {
    _formKey = value;
    notifyListeners();
  }

  FocusNode get movieNode => _movieNode;

  set movieNode(FocusNode value) {
    _movieNode = value;
    notifyListeners();
  }

  TextEditingController get movieController => _movieController;

  set movieController(TextEditingController value) {
    _movieController = value;
    notifyListeners();
  }

  MovieModel get movie => _movie;

  set movie(MovieModel value) {
    _movie = value;
    notifyListeners();
  }

  String? validaMovie(String movie) {
    if (movie.isEmpty) {
      return 'Error: campo vacío.';
    } else {
      return null;
    }
  }

  Future<void> buscarMovie(BuildContext context) async {
    myDebugPrint(_className, 'click buscarMovie');

    if (formKey.currentState!.validate()) {
      formKey.currentState!.save();
      movieNode.unfocus();

      _alert.loadingIndicator(context);

      movieController.text = movieController.text.trim();
      final movieFormated = movieController.text.replaceAll(' ', '+');

      _api.getMovie(movieFormated).then((response) {
        myDebugPrint(_className, response.toString());
        Navigator.pop(context);

        if (response['Response'] == 'True') {
          movie = MovieModel();

          movie.title = response['Title'];
          movie.year = response['Year'];
          movie.released = response['Released'];
          movie.type = response['Type'];
          movie.director = response['Director'];
          movie.poster = response['Poster'];

          if (response['Type'] == 'series') {
            movie.seasons = response['totalSeasons'];
          } else {
            movie.seasons = '';
          }

          movieController.clear();
          Navigator.pushNamed(context, MovieDetailPage.route);
        } else {
          const snackBar = SnackBar(
            backgroundColor: cErrorColor,
            content: Text('Error: no se pudo localizar la película ingresada, intenta con otro título.'),
          );
          ScaffoldMessenger.of(context).showSnackBar(snackBar);
        }
      }).catchError((onError) {
        myDebugPrint(_className, onError.toString());
        Navigator.pop(context);

        final snackBar = SnackBar(backgroundColor: cErrorColor, content: Text(onError.toString()));
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      });
    }
  }
}

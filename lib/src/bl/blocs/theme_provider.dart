import 'package:flutter/material.dart';

class ThemeProvider extends ChangeNotifier {
  bool isDark(BuildContext context) {
    return (Theme.of(context).brightness == Brightness.dark);
  }
}

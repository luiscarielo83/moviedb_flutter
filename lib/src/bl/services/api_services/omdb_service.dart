import 'dart:convert';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:http/http.dart' as http;
import 'package:internet_connection_checker/internet_connection_checker.dart';

import '../../../utils/strings.dart';

class OmdbService {
  final _className = 'OmdbService';

  Future<Map<String, dynamic>> getMovie(String pelicula) async {
    myDebugPrint(_className, 'INVOCANDO SERVICIO $sOmdbApi&t=$pelicula');

    String url = '$sOmdbApi&t=$pelicula';
    Map<String, dynamic> data = {};

    try {
      final response = await http.get(Uri.parse(url), headers: sHeaders).timeout(const Duration(seconds: 10));
      data = json.decode(utf8.decode(response.bodyBytes));
    } catch (e, stackTrace) {
      myDebugPrint(_className, 'ERROR: $e');
      throw 'ERROR:\nNo se ha podido establecer conexión con el servidor, intente más tarde.';
    }

    //myDebugPrint(_className, 'DATOS_PELICULA: $data');
    return data;
  }

  // Método para checar si hay conexión a internet.
  Future<bool> hasInternet() async {
    final conectividad = await Connectivity().checkConnectivity();

    if (conectividad == ConnectivityResult.mobile) {
      // Estoy conectado a los datos, asegurarse que tenga internet.
      if (await InternetConnectionChecker().hasConnection) {
        // Datos móviles detectados y conexión a internet confirmada.
        return true;
      } else {
        // Datos móviles detectados pero sin conexión a internet.
        myDebugPrint(_className, 'SIN_DATOS_STATUS: ${await InternetConnectionChecker().connectionStatus}');
        return false;
      }
    } else if (conectividad == ConnectivityResult.wifi) {
      // Estoy conectado al WiFi, asegurarse que tenga internet.
      if (await InternetConnectionChecker().hasConnection) {
        // WiFi detectado y conexión a internet confirmada.
        return true;
      } else {
        // WiFi detectado pero sin conexión a internet.
        myDebugPrint(_className, 'SIN_WIFI_STATUS: ${await InternetConnectionChecker().connectionStatus}');
        return false;
      }
    } else {
      // No se detectaron ni datos móviles ni WiFi. No se detectó internet.
      myDebugPrint(_className, 'SIN_WIFI_DATOS_STATUS: ${await InternetConnectionChecker().connectionStatus}');
      return false;
    }
  }
}

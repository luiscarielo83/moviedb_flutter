import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../bl/blocs/movie_search_provider.dart';
import '../../utils/colors.dart';

class MovieSearchPage extends StatelessWidget {
  const MovieSearchPage({Key? key}) : super(key: key);

  static const String route = 'movie_search_route';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Movie Search')),
      body: const _Body(),
    );
  }
}


class _Body extends StatelessWidget {
  const _Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      physics: const BouncingScrollPhysics(),
      children: const [SizedBox(height: 8), _Imagen(), SizedBox(height: 48), _Formulario()],
    );
  }
}

class _Imagen extends StatelessWidget {
  const _Imagen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0,
      color: cWhiteColor,
      margin: const EdgeInsets.symmetric(horizontal: 32),
      child: Hero(tag: 'app_icon', child: Image.asset('assets/images/app_icon_ios.png', fit: BoxFit.fitWidth, height: 200)),
    );
  }
}

class _Formulario extends StatelessWidget {
  const _Formulario({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final msp = Provider.of<MovieSearchProvider>(context, listen: false);

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24),
      child: Form(
        key: msp.formKey,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: TextFormField(
                controller: msp.movieController,
                decoration: const InputDecoration(
                  labelText: 'Película',
                  prefixIcon: Icon(Icons.movie_creation_rounded),
                ),
                focusNode: msp.movieNode,
                keyboardType: TextInputType.text,
                onEditingComplete: () => msp.buscarMovie(context),
                onSaved: (_) => msp.movieController.text = _!,
                textInputAction: TextInputAction.search,
                validator: (_) => msp.validaMovie(_!),
              ),
            ),
            Hero(
              tag: 'poster',
              child: Container(
                decoration: const BoxDecoration(
                  color: cPrimaryColor,
                  borderRadius: BorderRadius.only(topRight: Radius.circular(8), bottomRight: Radius.circular(8)),
                ),
                padding: EdgeInsets.zero,
                child: IconButton(
                  onPressed: () => msp.buscarMovie(context),
                  padding: EdgeInsets.zero,
                  icon: const Icon(Icons.search_rounded, color: cWhiteColor),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

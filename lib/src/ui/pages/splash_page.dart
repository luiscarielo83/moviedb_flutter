import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../bl/blocs/splash_provider.dart';
import '../../utils/strings.dart';

class SplashPage extends StatelessWidget {
  const SplashPage({Key? key}) : super(key: key);

  static const String route = 'splash_route';
  final _className = 'SplashPage';

  @override
  Widget build(BuildContext context) {
    myDebugPrint(_className, 'build()');
    Future.delayed(Duration.zero, () => Provider.of<SplashProvider>(context, listen: false).moveToMainPage(context));

    return const _BodySplashPage();
  }
}

class _BodySplashPage extends StatelessWidget {
  const _BodySplashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MediaQuery(
      data: MediaQuery.of(context).copyWith(textScaleFactor: 1),
      child: Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const [_LogoApp(), SizedBox(height: 32), _Iniciando()],
          ),
        ),
      ),
    );
  }
}

class _LogoApp extends StatelessWidget {
  const _LogoApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Hero(tag: 'app_icon', child: Image.asset('assets/images/app_icon.png', height: 180, width: 180, fit: BoxFit.scaleDown));
}

class _Iniciando extends StatelessWidget {
  const _Iniciando({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedTextKit(
      animatedTexts: [
        TypewriterAnimatedText(
          'Iniciando',
          speed: const Duration(milliseconds: 150),
          textAlign: TextAlign.center,
        ),
      ],
    );
  }
}

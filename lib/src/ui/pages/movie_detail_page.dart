import 'dart:io';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../bl/blocs/movie_search_provider.dart';
import '../../utils/colors.dart';

class MovieDetailPage extends StatelessWidget {
  const MovieDetailPage({Key? key}) : super(key: key);

  static const String route = 'movie_detail_route';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(leading: const _Back(), title: const Text('Movie Detail')),
      body: const _Body(),
    );
  }
}

class _Back extends StatelessWidget {
  const _Back({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return (Platform.isAndroid)
        ? IconButton(onPressed: () => Navigator.pop(context), icon: const Icon(Icons.arrow_back, color: cWhiteColor))
        : IconButton(onPressed: () => Navigator.pop(context), icon: const Icon(Icons.arrow_back_ios, color: cWhiteColor));
  }
}

class _Body extends StatelessWidget {
  const _Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: const EdgeInsets.all(16),
      children: const [
        _Poster(),
        SizedBox(height: 24),
        _Titulo(),
        SizedBox(height: 8),
        _Tipo(),
        SizedBox(height: 8),
        _Estreno(),
        SizedBox(height: 8),
        _Director(),
        SizedBox(height: 8),
        _Temporadas(),
      ],
    );
  }
}

class _Poster extends StatelessWidget {
  const _Poster({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final msp = Provider.of<MovieSearchProvider>(context, listen: false);

    return Hero(
      tag: 'poster',
      child: Center(
        child: Card(
          child: ClipRRect(
            borderRadius: const BorderRadius.all(Radius.circular(8)),
            child: Image.network(msp.movie.poster!, fit: BoxFit.contain, height: 200),
          ),
        ),
      ),
    );
  }
}

class _Titulo extends StatelessWidget {
  const _Titulo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final msp = Provider.of<MovieSearchProvider>(context, listen: false);

    return RichText(
      text: TextSpan(
        text: 'Título:  ',
        style: const TextStyle(color: cBlackColor, fontFamily: 'OpenSans', fontWeight: FontWeight.w700),
        children: [TextSpan(text: msp.movie.title!, style: const TextStyle(fontWeight: FontWeight.w300))],
      ),
    );
  }
}

class _Tipo extends StatelessWidget {
  const _Tipo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final msp = Provider.of<MovieSearchProvider>(context, listen: false);

    return RichText(
      text: TextSpan(
        text: 'Tipo:  ',
        style: const TextStyle(color: cBlackColor, fontFamily: 'OpenSans', fontWeight: FontWeight.w700),
        children: [TextSpan(text: msp.movie.type!, style: const TextStyle(fontWeight: FontWeight.w300))],
      ),
    );
  }
}

class _Estreno extends StatelessWidget {
  const _Estreno({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final msp = Provider.of<MovieSearchProvider>(context, listen: false);

    return RichText(
      text: TextSpan(
        text: 'Estreno:  ',
        style: const TextStyle(color: cBlackColor, fontFamily: 'OpenSans', fontWeight: FontWeight.w700),
        children: [TextSpan(text: msp.movie.released!, style: const TextStyle(fontWeight: FontWeight.w300))],
      ),
    );
  }
}

class _Director extends StatelessWidget {
  const _Director({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final msp = Provider.of<MovieSearchProvider>(context, listen: false);

    return RichText(
      text: TextSpan(
        text: 'Director:  ',
        style: const TextStyle(color: cBlackColor, fontFamily: 'OpenSans', fontWeight: FontWeight.w700),
        children: [TextSpan(text: msp.movie.director!, style: const TextStyle(fontWeight: FontWeight.w300))],
      ),
    );
  }
}

class _Temporadas extends StatelessWidget {
  const _Temporadas({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final msp = Provider.of<MovieSearchProvider>(context, listen: false);

    return Visibility(
      visible: msp.movie.seasons != '' ? true : false,
      child: RichText(
        text: TextSpan(
          text: 'Temporadas:  ',
          style: const TextStyle(color: cBlackColor, fontFamily: 'OpenSans', fontWeight: FontWeight.w700),
          children: [TextSpan(text: msp.movie.seasons!, style: const TextStyle(fontWeight: FontWeight.w300))],
        ),
      ),
    );
  }
}

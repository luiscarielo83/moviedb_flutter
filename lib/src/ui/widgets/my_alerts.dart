import 'package:flutter/material.dart';
import 'package:loading_indicator/loading_indicator.dart';

import '../../utils/colors.dart';

class MyAlerts {
  loadingIndicator(BuildContext context) {
    return showDialog(
        context: context,
        barrierColor: cWhiteColor.withOpacity(0.6),
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            backgroundColor: cTransparentColor,
            elevation: 0,
            content: Container(
              width: 60,
              height: 60,
              decoration: const BoxDecoration(color: cTransparentColor, shape: BoxShape.circle),
              //padding: const EdgeInsets.all(16),
              child: const LoadingIndicator(indicatorType: Indicator.ballScale, colors: [cSecondaryColor]),
            ),
          );
        }).then((value) {}).catchError((error) {});
  }
}

class MyAlert extends StatelessWidget {
  final Color? backgroundColor;
  final Widget? title;
  final Widget? content;
  final VoidCallback? onPressed;

  const MyAlert({Key? key, this.backgroundColor, this.title, this.content, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MediaQuery(
      data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
      child: AlertDialog(
        backgroundColor: backgroundColor,
        title: title,
        content: content,
        actionsPadding: const EdgeInsets.only(right: 10, bottom: 10),
        actions: [
          ElevatedButton(
            onPressed: onPressed,
            style: ButtonStyle(backgroundColor: MaterialStateProperty.all(cWhiteColor)),
            child: const Padding(padding: EdgeInsets.symmetric(vertical: 12, horizontal: 40), child: Text('Aceptar')),
          ),
        ],
      ),
    );
  }
}

class MyDeleteAlert extends StatelessWidget {
  final Color? backgroundColor;
  final Widget? title;
  final Widget? content;
  final VoidCallback? onPressed;

  const MyDeleteAlert({Key? key, this.backgroundColor, this.title, this.content, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MediaQuery(
      data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
      child: AlertDialog(
        backgroundColor: backgroundColor,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(24)),
        title: title,
        content: content,
        actionsPadding: const EdgeInsets.only(top: 24, bottom: 24),
        actionsAlignment: MainAxisAlignment.center,
        actions: [
          OutlinedButton(
            onPressed: () => Navigator.of(context).pop(),
            child: const Padding(
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
              child: Text('Cancelar', style: TextStyle(color: cWhiteColor)),
            ),
            style: ButtonStyle(side: MaterialStateProperty.all(const BorderSide(color: cWhiteColor))),
          ),
          OutlinedButton(
            onPressed: onPressed,
            child: const Padding(
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
              child: Text('Eliminar', style: TextStyle(color: cPrimaryColor)),
            ),
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all(cWhiteColor),
              side: MaterialStateProperty.all(const BorderSide(color: cWhiteColor)),
            ),
          ),
        ],
      ),
    );
  }
}

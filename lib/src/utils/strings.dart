import 'dart:convert';

import 'package:flutter/material.dart';

/// TODO: app
const sAppName = 'MovieDB';

/// TODO: omdb_service.dart
const String sOmdbKey = 'ad28827d';
const String sOmdbApi = 'http://www.omdbapi.com/?apikey=$sOmdbKey';
const Map<String, String> sHeaders = {"Accept": "*/*", "Content-Type": "application/x-www-form-urlencoded"};
Encoding? sEncoding = Encoding.getByName("utf-8");

void myDebugPrint(String className, String message) {
  debugPrint('[$className] -> $message');
}

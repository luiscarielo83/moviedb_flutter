import 'package:flutter/material.dart';

/// [Oficial]
const cPrimaryColor = Color(0xFF5AACD8);
const cSecondaryColor = Color(0xFF93D282);

/// [Light]
final cTextLightColor = const Color(0xFF000000).withOpacity(0.9);
const cSelectionLightColor = Color(0xFFCDE6F3);

/// [Dark]
const cDarkBackground = Color(0xFF383838);
final cTextDarkColor = const Color(0xFFFFFFFF).withOpacity(0.9);

/// [AppBar]
const cTitleAppBarColor = Color(0xFFFFFFFF);

/// [Otros]
const cTransparentColor = Color(0x00000000);
const cBlackColor = Color(0xFF000000);
const cWhiteColor = Color(0xFFFFFFFF);
const cGreyColor = Color(0xFF9E9E9E);
const cGoodColor = Color(0xFF00c853);
const cWarningColor = Color(0xFFF6CB31);
const cErrorColor = Color(0xFFD32F2F);
const cScaffoldColor = Color(0xfff6faff);

/// [Gradiente]
const cBeginColor = Color(0xFF7abcdf);
const cMiddleLightColor = Color(0xFF6ab4db);
const cMiddleColor = Color(0xFF5AACD8);
const cMiddleDarkColor = Color(0xFF5aacd8);
const cEndColor = Color(0xFF519ac2);

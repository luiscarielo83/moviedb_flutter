import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'src/app.dart';

void main() async {
  // Asegurar que en la app inicien los Widgets correctamente.
  WidgetsFlutterBinding.ensureInitialized();

  // Mantener la orientación del dispositivo en forma vertical.
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

  runApp(const App());
}
